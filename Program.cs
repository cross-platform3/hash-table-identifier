using System;
using System.Collections.Generic;

public class HashTable
{
    private const int TableSize = 100; // Розмір хеш-таблиці
    private List<KeyValuePair<int, string>>[] table;

    public HashTable()
    {
        table = new List<KeyValuePair<int, string>>[TableSize];
    }

    // Функція хешування
    private int HashFunction(int key)
    {
        return key % TableSize;
    }

    // Додати елемент до таблиці
    public void Add(int key, string value)
    {
        int index = HashFunction(key);
        if (table[index] == null)
        {
            table[index] = new List<KeyValuePair<int, string>>();
        }
        table[index].Add(new KeyValuePair<int, string>(key, value));
    }

    // Пошук елемента за ключем
    public string Search(int key)
    {
        int index = HashFunction(key);
        if (table[index] != null)
        {
            foreach (var pair in table[index])
            {
                if (pair.Key == key)
                {
                    return pair.Value;
                }
            }
        }
        return null; // Якщо елемент не знайдено
    }
}

class Program
{
    static void Main(string[] args)
    {
        HashTable hashTable = new HashTable();

        // Додати елементи до таблиці
        hashTable.Add(101, "variable1");
        hashTable.Add(202, "my_varible");
        hashTable.Add(303, "your_varible");

        // Пошук елементів
        Console.WriteLine("Знайдено значення для ключа 101: " + hashTable.Search(101));
        Console.WriteLine("Знайдено значення для ключа 202: " + hashTable.Search(202));
        Console.WriteLine("Знайдено значення для ключа 303: " + hashTable.Search(303));
        Console.WriteLine("Знайдено значення для ключа 404: " + hashTable.Search(404)); // Повинно вивести null
    }
}